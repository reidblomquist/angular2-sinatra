require 'sinatra'
require 'sinatra/json'

db = []

set :public_folder, File.dirname(__FILE__) + "/public"

idx = 0

get '/api/fights' do
  json db
end

post '/api/fights' do
  idx = idx + 1
  obj = { id: idx, fighter1: params[:fighter1], fighter2: params[:fighter2], won: params[:won] }
  db << obj
  json obj
end

put '/api/fights/:id' do
  db.each { |x|
  	if (x[:id] == params[:id].to_i)
  		x[:fighter1] = params[:fighter1]
  		x[:fighter2] = params[:fighter2]
  		x[:won] = params[:won]
  	end
  }
end

delete '/api/fights/:id' do
  db.delete_if { |x| x[:id] == params[:id].to_i }
  json 'Ok'
end
